#
# Filename: Makefile
# Author: Iván Ruvalcaba
# Contact: <ivanruvalcaba[at]disroot[dot]org>
# Created: 21 jun. 2019 14:28:56
# Last Modified: 29 jun 2019 12:32:09
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#

TXT2TAGS = python2 -m txt2tags
DOCUMENT_TYPE = html
PUBLIC_DIR = public_html
ASSETS_DIR = docs/assets
DOCUMENT = index

# TODO: Aspectos a mejorar del Makefile
#
# - Revisar si el siguiente flujo de trabajo es el adecuado.

all: clean init build

init:
	mkdir -p ${PUBLIC_DIR}
	cp -r ${ASSETS_DIR} ${PUBLIC_DIR}

build:
	@echo "Converting txt2tags to ${DOCUMENT_TYPE}."
	$(TXT2TAGS) --target $(DOCUMENT_TYPE) --outfile $(PUBLIC_DIR)/$(DOCUMENT).html docs/$(DOCUMENT).t2t

clean:
	rm -rf ${PUBLIC_DIR}
