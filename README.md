[//]: # (Filename: README.md)
[//]: # (Author: Iván Ruvalcaba)
[//]: # (Contact: <ivanruvalcaba[at]disroot[dot]org>)
[//]: # (Created: 24 Jun 2019 20:03:36)
[//]: # (Last Modified: 25 Jun 2019 14:54:53)

# txt2tags notepad

> Block de notas personal sobre _txt2tags_.

Este proyecto tiene como finalidad principal el recopilar
funcionalidades, tips y experiencia práctica empleando **txt2tags** como
block de notas/wiki personal.

## Licencias

Este proyecto cuenta con una **licencia dual**. A continuación más
detalles al respecto:

### Documentación

- Licencia Creative Commons Atribución-CompartirIgual 4.0 Internacional
  ([CC BY-SA 4.0
  International](https://creativecommons.org/licenses/by-sa/4.0/)).

### Código fuente (software)

- Licencia Pública de Mozilla 2.0
  ([MPL-2.0](https://www.mozilla.org/en-US/MPL/2.0/)).

## Autor

Copyright (c) 2019 Iván Ruvalcaba <ivanruvalcaba[at]disroot[dot]org>

## Changelog

- Implementación de un archivo _Makefile_ para facilitar el proceso de
  conversión del lenguaje de marcado _txt2tags_ a _HTML_. Incluyendo el
  copiado de _assets_.
- Aplicar hoja de estilo al archivo _HTML_ utilizando _skeleton 2_.
- Definición de los tipos de licencia del proyecto, siendo imperativo
  recurrir a un licenciamiento dual —una licencia para la documentación
  y otra correspondiente para el código fuente que requiera el proyecto
  para su gestión—.
- Implementación de _Browser-Sync_ para facilitar la labor de revisión
  de la documentación.
- Inclusión de un sistema de control de versiones.

## TODO

- [ ] _Makefile:_ Refactorizar archivo.
- [ ] _Browser-sync:_ Crear archivo de configuración.
- [ ] Definir el _scaffolding_ idóneo para la documentación.
